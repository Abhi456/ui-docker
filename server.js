'use strict';

const express = require('express');

// Constants
const port = 8080;

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello World');
});

app.listen(port, () => console.log(`Listening on port ${port}`));